public class Student {
    private String fio;
    private String groupNum;
    private double[] uspevaemost;
    public Student(String fio, String groupNum, double[] uspevaemost){
        this.fio = fio;
        this.groupNum = groupNum;
        this.uspevaemost = uspevaemost;
    }
    public String getFio(){
        return fio;
    }
    public void setFio(String fio){
        this.fio = fio;
    }
    public String getGroupNum(){
        return groupNum;
    }
    public void setGroupNum(String groupNum){
        this.groupNum = groupNum;
    }
    public double[] getUspevaemost(){
        return uspevaemost;
    }
    public void setUspevaemost(double[] uspevaemost){
        this.uspevaemost = uspevaemost;
    }
    public double getSrBall() {
        double sum = 0;
        int k = 0;
        for (int i = 0; i < 5; i++) {
            sum += uspevaemost[i];
            k++;
        }
        return sum / k;
    }
    @Override
    public String toString() {
        return String.format("ФИО: %s Группа: %s Успеваемость: %f, %f, %f, %f, %f Средний балл, %f",fio,groupNum,uspevaemost[0],uspevaemost[1],uspevaemost[2],uspevaemost[3],uspevaemost[4],getSrBall());
    }
}
